var AlchemyApp = {
    state: {
        params: undefined,
        step: 0,
        valid: false,
        success: false,
        isFinalPhase: function() { return this.step >= AlchemyApp.config.steps * 0.75 }
    },

    config: {
        interval: 100,
        steps: 100,
        renderers: [SensorsRenderer(), ProgressRenderer(), RainbowRenderer()]
    },

    initialize: function () {
        var self = this;
        _.each(this.config.renderers, function(item) {
            item.initialize(self.config)
        })
    },

    reset: function () {
        this.state.step = 0;
        this.state.params = undefined;
        this.state.valid = false;
        this.state.success = false;
        var self = this;
        _.each(this.config.renderers, function (item) {
            item.reset(self.config)
        })
    },

    run: function (params) {
        if (!_.isUndefined(params)) {
            if (_.isUndefined(this.CHANCES[params.action]) ||
                _.isUndefined(this.CHANCES[params.action][params.source]) ||
                _.isUndefined(this.CHANCES[params.action][params.source][params.qty])) {
                $('#results').modal({show: true, keyboard: true});
                return
            }

            this.state.params = params;
            this.state.valid = true;

            var chance = this.CHANCES[params.action][params.source][params.qty];
            this.state.success = Math.random() < chance;

            // debug
            console.log(params);
            console.info('chance = ' + chance);
            console.info('res = ' + this.state.success );
            //this.state.success = true;
            //this.state.step = 100;
            // ---

            this.setTimeout();
        }
    },

    setTimeout: function () {
        setTimeout(function () {
            AlchemyApp.tic()
        }, this.config.interval)
    },

    tic: function () {
        var self = this;
        _.each(this.config.renderers, function(item) {
            item.render(self.state, self.config)
        });

        this.state.step += 1;
        if (this.state.step < this.config.steps)
            this.setTimeout();
        else
            $('#results').modal({show: true, keyboard: true});
    },

    CHANCES: {
        'create': {
            'simple': {
                '2': .8,
                '3': .87,
                '4': .9
            },
            'medium': {
                '3': .7,
                '4': .75,
                '5': .8
            },
            'nice': {
                '4': .6,
                '5': .65,
                '6': .7
            }
        },
        'clean': {
            'simple': {
                '4': .45,
                '5': .5,
                '6': .55
            },
            'medium': {
                '4': .55,
                '5': .6,
                '6': .65
            },
            'nice': {
                '2': .7,
                '3': .75,
                '4': .8
            }
        }
    }
};