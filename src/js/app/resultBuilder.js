var ResultBuilder = {
    invalid: function () {
        return '<b>Ошибка!</b> Неверные параметры для запуска!'
    },
    fail: function () {
        return '<b>Неудача!</b> Не удалось ' + this._action() + '! Материалы были утеряны.';
    },

    success: function () {
        if(AlchemyApp.state.params.action != 'create')
            return '<b>Успех!</b> Негативный эффект успешно снят.';

        var text = '<b>Успех!</b> Был получен новый активный артефакт: ' + this._artifact();

        return text;
    },

    _artifact: function() {
        if(AlchemyApp.state.params.source == 'simple')
            return '<b>Ценный артефакт</b>.';
        if(AlchemyApp.state.params.source == 'medium')
            return '<b>Редкий артефакт</b>.';

        var text = 'Уникальный артефакт с постоянным эффектом.<br/>';
        var results = _.sample(_.range(this.effects.positive.length), 3);
        console.log(results)
        for(var i = 0; i < results.length; i++) {
            text += ('<br/>' + (i == 0 ?
                this.effects.positive[results[i]] : this.effects.negative[results[i]]));
        }
        return text
    },


    _action: function () {
        return AlchemyApp.state.params.action == 'create' ? 'выполнить преобразование' :
            'снять негативный эффект';
    },

    effects: {
        positive: [
            'Ускорение перезарядки на (1/2/3 с)',
            'Индекс брони +20',
            'Шок - 0.5с',
            'Неуязвимость + 0.5с',
            'Ношение магазинов +5'

        ],
        negative: [
            'Замедление перезарядки на (1/2/3 с)',
            'Индекс брони -10',
            'Шок + 0.25с',
            'Неуязвимость - 0.25с',
            'Ношение магазинов -3'
        ]
    }
};