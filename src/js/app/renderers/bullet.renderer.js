function BulletRenderer() {

    var data = [
        {
            "title": "Revenue",
            "subtitle": "US$, in thousands",
            "ranges": [150, 225, 300],
            "measures": [220, 270],
            "markers": [250]
        },
        {
            "title": "Profit",
            "subtitle": "%",
            "ranges": [20, 25, 30],
            "measures": [21, 23],
            "markers": [26]
        },
        {
            "title": "Order Size",
            "subtitle": "US$, average",
            "ranges": [350, 500, 600],
            "measures": [100, 320],
            "markers": [550]
        },
        {
            "title": "New Customers",
            "subtitle": "count",
            "ranges": [1400, 2000, 2500],
            "measures": [1000, 1650],
            "markers": [2100]
        },
        {
            "title": "Satisfaction",
            "subtitle": "out of 5",
            "ranges": [3.5, 4.25, 5],
            "measures": [3.2, 4.7],
            "markers": [4.4]
        }
    ];

    var margin = {top: 5, right: 40, bottom: 20, left: 120},
        width = 960 - margin.left - margin.right,
        height = 50 - margin.top - margin.bottom;

    var chart = d3.bullet()
        .width(width)
        .height(height);

    var svg;

        svg = d3.select("#bullets").selectAll("svg")
            .data(data)
            .enter().append("svg")
            .attr("class", "bullet")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
            .call(chart);

        var title = svg.append("g")
            .style("text-anchor", "end")
            .attr("transform", "translate(-6," + height / 2 + ")");

        title.append("text")
            .attr("class", "title")
            .text(function (d) {
                return d.title;
            });

        title.append("text")
            .attr("class", "subtitle")
            .attr("dy", "1em")
            .text(function (d) {
                return d.subtitle;
            });

    function randomize(d) {
        if (!d.randomizer) d.randomizer = randomizer(d);
        d.ranges = d.ranges.map(d.randomizer);
        d.markers = d.markers.map(d.randomizer);
        d.measures = d.measures.map(d.randomizer);
        return d;
    }

    function randomizer(d) {
        var k = d3.max(d.ranges) * .2;
        return function (d) {
            return Math.max(0, d + k * (Math.random() - .5));
        };
    }

    // Inherited part
    return {
        initialize: function (config) {
            svg.datum(randomize).call(chart.duration(1000));
        },
        reset: function (config) {
            this.currentLevel = 0
        },
        render: function (state, config) {
            var level = Math.ceil(10 * state.step / config.steps);
            if (level > this.currentLevel) {
                this.currentLevel = level;
                svg.datum(randomize).call(chart.duration(1000));
            }
        },

        // special part
        currentLevel: 0

    }
}