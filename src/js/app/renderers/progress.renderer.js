function ProgressRenderer() {

    var GREEN_CSS_CLASS = 'green';
    var RED_CSS_CLASS = 'red';
    var YELLOW_CSS_CLASS = 'yellow';

    return {
        // Inherited part
        initialize: function (config) {
            this.progressBar = radialProgress(this.wrapper.get(0))
                .label("Complete")
                .diameter(300);
            this.reset(config)
        },
        reset: function (config) {
            this.wrapper
                .removeClass(GREEN_CSS_CLASS)
                .removeClass(YELLOW_CSS_CLASS)
                .addClass(RED_CSS_CLASS);
            this.progressBar
                .maxValue(config.steps)
                .value(0)
                .render()
        },
        render: function (state, config) {
            var value = state.step + 1;
            this.wrapper.addClass(value > config.steps * 0.8 ? GREEN_CSS_CLASS :
                value > config.steps * 0.3 ? YELLOW_CSS_CLASS : '');

            if (value % 5 == 0)
                this.progressBar
                    .value(value)
                    .render()
        },

        // special part
        progressBar: null,
        wrapper: jQuery('#radialProgress')
    }
}