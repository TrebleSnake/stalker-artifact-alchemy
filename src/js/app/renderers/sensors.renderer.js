function SensorsRenderer() {

    var DEFAULT_VALUE = 0;
    var MAX_DIFF = 3;
    var MAX_VALUE = 100;
    var DANGER_ZONE = 0.9;
    var CONTAINER_POSTFIX = "SensorContainer";
    var SENSORS = ["Σ ρ", "Нагрузка", "Обороты", "π·μ / U"]

    return {
        // Inherited part
        initialize: function (config) {
            var self = this;
            var i = 0;
            _.each(SENSORS, function(item){
                i++;
                var name = 'sensor' + i;
                jQuery("#sensors").append('<div class="sensor" id="' + name + CONTAINER_POSTFIX + '"></div>')
                self.createGauge(name, item);
            });
        },
        render: function (state, config) {
            var self = this;
            _.each(this.sensors, function(sensor) {
                self.update(sensor, sensor.value += self.getRandomValue(state, config, sensor.value));
            })
        },
        reset: function(config) {
            var self = this;
            _.each(this.sensors, function(sensor) {
                self.update(sensor, 0, 2000);
            })
        },

        // Special part
        sensors: [],

        update: function(sensor, value, duration) {
            sensor.value = value;
            sensor.view.redraw(sensor.value, _.isUndefined(duration) ? 100 : duration);
        },

        createGauge: function (name, label, min, max) {
            var config =
            {
                size: 120,
                label: label,
                min: undefined != min ? min : 0,
                max: undefined != max ? max : MAX_VALUE,
                minorTicks: 5
            };

            var range = config.max - config.min;
            config.yellowZones = [{
                from: config.min + range * 0.75,
                to: config.min + range * DANGER_ZONE
            }];
            config.redZones = [{from: config.min + range * DANGER_ZONE, to: config.max}];

            var sensor = new Gauge(name + CONTAINER_POSTFIX, config);
            sensor.render();

            this.sensors.push({
                view: sensor,
                value: DEFAULT_VALUE
            })
        },

        getRandomValue: function (state, config, currentValue) {
            if (currentValue < MAX_VALUE * 0.4)
                return Math.round(MAX_VALUE * 0.1 * Math.random() + MAX_VALUE * 0.1);

            var sign = state.isFinalPhase() && !state.success ? 1 :
                currentValue >= MAX_VALUE * DANGER_ZONE ? -1 :
                    Math.random() > 0.4 ? +1 : -1;

            return Math.round(currentValue == MAX_VALUE ? -1 :
            sign * _.min([MAX_VALUE - currentValue, MAX_DIFF]) * Math.random())
        }

    }
}