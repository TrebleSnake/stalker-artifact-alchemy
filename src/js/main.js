$(document).ready(function(){

    var app = AlchemyApp;
    app.initialize();

    $("#run").click(function () {
        app.run({
            action: $("#actionType").find(".active input").val(),
            source: $("#sourceType").find(".active input").val(),
            qty: $("#sourceQty").find(".active input").val()
        })
    });

    $('#results').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('#resultsText')
            .removeClass('alert-danger')
            .removeClass('alert-warning')
            .removeClass('alert-success');

        if(!AlchemyApp.state.valid) {
            modal.find('#resultsText').addClass('alert-warning')
                .html(ResultBuilder.invalid());
            AlchemyApp.reset();
            return;
        }

        if(!AlchemyApp.state.success) {
            modal.find('#resultsText').addClass('alert-danger')
                .html(ResultBuilder.fail());
            AlchemyApp.reset();
            return;
        }

        modal.find('#resultsText').addClass('alert-success')
            .html(ResultBuilder.success());
        AlchemyApp.reset();
    });

    $('#actionType, #sourceType').find('label.btn').click(function () {
        var isAction = $(this).parent().attr('id') == 'actionType';
        // эти извращения нужны, т.к. не всегда Bootstrap срабатывает быстрее этой ф-ции
        var action = isAction ? $(this).find('input').val() : $("#actionType").find(".active input").val();
        var source = !isAction ? $(this).find('input').val() : $("#sourceType").find(".active input").val();
        var rule = rules[action][source];

        if (!_.isUndefined(rule))
            for (var i = 2; i <= 6; i++) {
                var q = $('#sourceQty').find('#q'+i).parent();
                if(_.contains(rule, i))
                    q.removeClass('disabled');
                else
                    q.addClass('disabled')
            }
    });

    var rules = {
        'create': {
            'simple': [2, 3, 4],
            'medium': [3, 4, 5],
            'nice': [4, 5, 6]
        },
        'clean': {
            'simple': [4, 5, 6],
            'medium': [4, 5, 6],
            'nice': [2, 3, 4]
        }
    }

});
